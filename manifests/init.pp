class base (
  $package_ensure = $base::params::package_ensure,
) inherits base::params {
  package { 
    [
        'build-essential',
        'python-software-properties',
        'zip',
        'git',
    ]:
    ensure  => $package_ensure,
  }
}
